### WinBUGS PKPD Model Library Example 1: Two compartment model

if(.Platform$OS.type == "windows") Sys.setenv(HOME = "c:")
problemDir = file.path(Sys.getenv("HOME"), 
	"BUGSModelLibrary/examples") # subdirectory containing the BUGS model file
toolsDir = problemDir
model.name = "twoCptExample" # root names of model file
bugsDir = "c:/Program Files/BlackBox Component Builder 1.5/"
wineBin = "/usr/local/MacPorts/bin" # directory containing wine and winepath programs. only relevant on unix or Mac OS X

setwd(problemDir)
library(R2WinBUGS)
library(coda)
library(lattice)
source(paste(toolsDir,"/bugs.tools.R",sep=""))
source(paste(toolsDir,"/bgillespie.utilities.R",sep=""))
set.seed(10271998) # not required but assures repeatable results

options(error = expression(NULL)) # prevents stopping for errors when running in batch
memory.limit(2048)

## get data files
xdata1 <- read.csv("fxa.data.csv",as.is=T)
xdata2 <- read.csv("fxa.data2.csv",as.is=T)
names(xdata2)[names(xdata2)=="patient"] <- "subject"
xdata1$cobs <- as.numeric(xdata1$cobs)
xdata2$cobs <- as.numeric(xdata2$cobs)

## Phase I data
x1 <- xdata1[c("subject", "weight", "age", "gender", "time",
               "dose", "cobs", "fxa.inh.obs")]
x1 <- x1[x1$dose > 0, ] # drop placebo patients
x1 <- x1[x1$time > 0, ] # time = 0 "data" are non-informative

x1$study <- rep(1,nrow(x1))
x1$evid <- rep(0,nrow(x1))
x1$ii <- rep(0,nrow(x1))
x1$addl <- rep(0,nrow(x1))
phase1.data <- x1[order(x1$subject,x1$time), ]

## dose records for Phase I study
phase1.dose <- phase1.data[!duplicated(phase1.data$subject), ]
phase1.dose$time <- rep(0,nrow(phase1.dose))
phase1.dose$evid <- rep(1,nrow(phase1.dose))
phase1.dose$cobs <- rep(NA,nrow(phase1.dose))

## Phase IIa data
x1 <- xdata2[c("subject", "weight", "age", "gender", "time",
               "drug", "dose", "cobs", "fxa.inh.obs")]
x1 <- x1[x1$drug == "ME-2",] # drop enoxaparin patients
x1 <- x1[x1$time > 0, ] # time = 0 "data" are non-informative

x1$study <- rep(2,nrow(x1))
x1$evid <- rep(0,nrow(x1))
x1$ii <- rep(0,nrow(x1))
x1$addl <- rep(0,nrow(x1))
x1$drug <- NULL
phase2.data <- x1[order(x1$subject,x1$time), ]
phase2.data$subject <- max(phase1.data$subject) + phase2.data$subject

## dose records for Phase II study
phase2.dose <- phase2.data[!duplicated(phase2.data$subject), ]
phase2.dose$time <- rep(0,nrow(phase2.dose))
phase2.dose$evid <- rep(1,nrow(phase2.dose))
phase2.dose$ii <- rep(12,nrow(phase2.dose))
phase2.dose$addl <- rep(13,nrow(phase2.dose))
phase2.dose$cobs <- rep(NA,nrow(phase2.dose))

xdata <- rbind(phase1.data, phase2.data, phase1.dose, phase2.dose)
xdata <- xdata[order(xdata$subject, xdata$time,1-xdata$evid), ]
xdata$seqID <- as.integer(as.factor(xdata$subject))
nobs <- nrow(xdata)
start <- (1:nobs)[!duplicated(xdata$subject)]

## create WinBUGS data set
bugsdata <- list(
                 nobs = nobs,
                 nsub = length(unique(xdata$subject)),
                 start = start,
                 end = c(start[-1]-1,nobs),
                 subject = xdata$seqID,
                 weight = xdata$weight,
                 time = xdata$time,
                 amt = xdata$dose,
                 rate = rep(0,nobs),
                 ii = xdata$ii,
                 evid = xdata$evid,
                 cmt = ifelse(xdata$evid > 0, 1, 2),
                 addl = xdata$addl,
                 ss = rep(0,nobs),
                 logCobs = ifelse(xdata$cobs <=0, NA, log(xdata$cobs)),
                 ##	fxa = xdata$fxa.inh.obs,
                 omega.inv.prior=diag(rep(0.05,5))
                 )

## create initial estimates
bugsinit <- function() {
    list(logCLHat = rnorm(1,log(10),0.2),
         logQHat = rnorm(1,log(20),0.2),
         logV1Hat = rnorm(1,log(70),0.2),
         logV2Hat = rnorm(1,log(70),0.2),
         logDkaHat = rnorm(1,log(1),0.2),
         omega.inv = solve(diag(exp(2*rnorm(5,log(0.25),0.5)))),
         sigma = runif(1,0.1,2)
         )
}

# specify what variables to monitor
parameters = c("CLHat","QHat","V1Hat","V2Hat","DkaHat","omega","sigma",
	"logCobsCond","logCobsPred")

# specify the variables for which you want history and density plots
parameters.to.plot = c("deviance","CLHat","QHat","V1Hat","V2Hat","DkaHat","omega","sigma")

################################################################################################
# run WinBUGS

n.chains = 3
n.iter = 10000
n.burnin = 4000
n.thin = 6
bugs.fit <- bugs(data = bugsdata, inits = bugsinit, parameters.to.sav = parameters,
	model.file = file.path(problemDir, paste(model.name, ".txt", sep="")),
	n.chains = n.chains, n.iter = n.iter, n.burnin = n.burnin, n.thin = n.thin, clearWD = T,
	bugs.directory = bugsDir, working.directory = getwd(),
	useWINE = (.Platform$OS.type == "unix"), WINE = file.path(wineBin,"wine"),
	newWINE = (.Platform$OS.type == "unix"), WINEPATH = file.path(wineBin,"winepath"))

# save scripts, data and results to a directory

save.model(bugs.fit, model.name)
#load(paste(model.name,"/",model.name,".fit.Rsave",sep=""))

# rename and reformat MCMC results to facilitate later calculations and plots

sims.array <- bugs.fit$sims.array
posterior <- array(as.vector(sims.array),dim=c(prod(dim(sims.array)[1:2]),dim(sims.array)[3]),
	dimnames=list(NULL,dimnames(sims.array)[[3]]))

################################################################################################
# posterior distributions of parameters

# open graphics device
pdf(file = paste(model.name,"/",model.name,".plots.pdf",sep=""),width=6,height=6)

# subset of sims.array containing selected variables
x1 <- sims.array[,,unlist(sapply(c(paste("^",parameters.to.plot,"$",sep=""),
	paste("^",parameters.to.plot,"\\[",sep="")),grep,x=dimnames(sims.array)[[3]]))]


## create history, density and Gelman-Rubin-Brooks plots, and a table of summary stats
ptable <- parameter.plot.table(x1)
write.csv(signif(ptable,3),paste(model.name,"/",model.name,".summary.csv",sep=""))

################################################################################################
# posterior predictive distributions of plasma concentrations

# prediction of future observations in the same subjects, i.e., posterior predictions
# conditioned on observed data from the same subject

pred = exp(posterior[,grep("logCobsCond\\[",dimnames(posterior)[[2]])])

x1 = xdata
x1$cobs = as.numeric(x1$cobs)
x1$type =rep("observed",nrow(x1))
x2 = rbind(x1,x1,x1)
x2$cobs = as.vector(t(apply(pred,2,quantile,probs=c(0.05,0.5,0.95))))
x2$type = rep(c("5%ile","median","95%ile"),ea=nrow(x1))
x1 = rbind(x1,x2)

doses = sort(unique(x1$dose[x1$study==1]))
for(dose in doses){
	print(xyplot(cobs~time|as.factor(subject),x1[x1$study==1&x1$dose==dose,],groups=type,layout=c(5,5),
		panel=panel.superpose,type=c("l","l","l","p"),lty=c(3,3,1,0),col=c(2,2,4,1),
		pch=c(NA,NA,NA,19),lwd=2,cex=0.5,scales=list(cex=1),
		xlab=list(label="dose",cex=1.2),ylab=list(label="concentration",
		cex=1.2),par.strip.text=list(cex=1),
		strip = function(...) strip.default(..., style = 1),
		main=list(label=paste("study 1  ",dose," mg",sep=""),cex=1.5),
		sub=list(label="individual predictions",cex=1.5)))
}

print(xyplot(cobs~time|as.factor(subject),x1[x1$study==2,],groups=type,layout=c(5,5),
	panel=panel.superpose,type=c("l","l","l","p"),lty=c(3,3,1,0),col=c(2,2,4,1),
	pch=c(NA,NA,NA,19),lwd=2,cex=0.5,scales=list(cex=1),
	xlab=list(label="dose",cex=1.2),ylab=list(label="concentration",
	cex=1.2),par.strip.text=list(cex=1),
	strip = function(...) strip.default(..., style = 1),
	main=list(label=paste("study 2 20 mg",sep=""),cex=1.5),
	sub=list(label="individual predictions",cex=1.5)))

# prediction of future observations in a new subject, i.e., posterior predictive distributions

pred = exp(posterior[,grep("logCobsPred\\[",dimnames(posterior)[[2]])])

x1 = xdata
x1$cobs = as.numeric(x1$cobs)
x1$type =rep("observed",nrow(x1))
x2 = rbind(x1,x1,x1)
x2$cobs = as.vector(t(apply(pred,2,quantile,probs=c(0.05,0.5,0.95))))
x2$type = rep(c("5%ile","median","95%ile"),ea=nrow(x1))
x1 = rbind(x1,x2)

doses = sort(unique(x1$dose[x1$study==1]))
for(dose in doses){
	print(xyplot(cobs~time|as.factor(subject),x1[x1$study==1&x1$dose==dose,],groups=type,layout=c(5,5),
		panel=panel.superpose,type=c("l","l","l","p"),lty=c(3,3,1,0),col=c(2,2,4,1),
		pch=c(NA,NA,NA,19),lwd=2,cex=0.5,scales=list(cex=1),
		xlab=list(label="dose",cex=1.2),ylab=list(label="concentration",
		cex=1.2),par.strip.text=list(cex=1),
		strip = function(...) strip.default(..., style = 1),
		main=list(label=paste("study 1  ",dose," mg",sep=""),cex=1.5),
		sub=list(label="population predictions",cex=1.5)))
}

print(xyplot(cobs~time|as.factor(subject),x1[x1$study==2,],groups=type,layout=c(5,5),
	panel=panel.superpose,type=c("l","l","l","p"),lty=c(3,3,1,0),col=c(2,2,4,1),
	pch=c(NA,NA,NA,19),lwd=2,cex=0.5,scales=list(cex=1),
	xlab=list(label="dose",cex=1.2),ylab=list(label="concentration",
	cex=1.2),par.strip.text=list(cex=1),
	strip = function(...) strip.default(..., style = 1),
	main=list(label=paste("study 2 20 mg",sep=""),cex=1.5),
	sub=list(label="population predictions",cex=1.5)))

dev.off()

