\documentclass[12 pt ]{article}
%\usepackage[square,numbers]{natbib}
\usepackage{graphicx}
\usepackage{amsmath,amsthm,amsfonts}
\usepackage{appendix}
\usepackage{vaucanson-g}


\setlength{\parindent}{0in}
%\setlength{\textheight}{9.5in}
\setlength{\parskip}{0.15in}
%\setlength{\textwidth}{6.2in}
%\setlength{\oddsidemargin}{0in} \setlength{\evensidemargin}{0in}
%\addtolength{\topmargin}{-0.5in}
%\renewcommand{\baselinestretch}{1.0}

\begin{document}

\begin{titlepage}
\includegraphics[width = 2.5cm, scale = 1.0, bb = 0 0 304 463]{OpenBUGSlogo.bmp}
\vspace{0.5cm}
\begin{center}
{\LARGE
OpenBUGS Development Interface (UDev): \\
Implementing your own functions} \\
\Large{
\vspace{0.5cm}
Dave Lunn, Chris Jackson \& Steve Miller, \\
MRC Biostatistics Unit, \\
Institute of Public Health, \\
University Forvie Site,  \\
Robinson Way,  \\
Cambridge,  \\
CB2 0SR, \\
\texttt{david.lunn@mrc-bsu.cam.ac.uk}\\
\texttt{chris.jackson@mrc-bsu.cam.ac.uk}\\
\texttt{steve.miller@mrc-bsu.cam.ac.uk}
}

\vspace {1cm}
OpenBUGS\\
\texttt{http://sourceforge.net/projects/openbugs/}
\vfill
{\large \today}
\end{center}



\end{titlepage}
\tableofcontents
\newpage



\section{Introduction}
This document explains how you can implement arbitrarily complex
logical functions in OpenBUGS by `hard-wiring' them into the
system via compiled Component Pascal code. The facility to implement 
new distributions will be included in a future release. 

There are three main advantages
to doing this: first, `hard-wired' functions can be evaluated much
more quickly than their BUGS-language equivalents; second, the
full flexibility of a general-purpose computer language is
available for specifying the desired function, and so piecewise
functions, for example, can be specified straightforwardly whereas
their specification via the BUGS language (using the
\texttt{step(.)}~function) can be somewhat awkward; finally, the
practice of hiding the details of complex logical functions within
`hard-wired' components can lead to vastly simplified OpenBUGS code
for the required statistical model, which reduces the likelihood
of coding errors and is easier both to read and to modify.

We demonstrate how to implement such `hard-wired' components via a
worked example in which the following function becomes a single
element of the BUGS language.
\begin{equation}
C(t) = \left\{ \begin{array}{cc}
  0 & t < 0 \\
  \frac{D}{V} \frac{k_a}{k_a - k_e} \left[\exp(-k_{e} t) - \exp(-k_{a} t) \right] & t \geq 0 \\
\end{array}
\right. \label{PKFO1Equation}
\end{equation}
This is known in the field of pharmacokinetics as a ``one
compartment open model with first-order absorption''. Here
$C(t)$~denotes the concentration, at time $t$, of drug in blood
plasma following (oral) administration of a dose $D$; the system
parameters $V$, $k_e$ and $k_a$ denote the drug's volume of
distribution, elimination rate constant, and (first-order)
absorption rate constant, respectively. For reasons of
identifiability we parameterise the model in terms of $\theta =
\log(V, k_{e}, k_{a} - k_{e})$  so every possible combination
of real values (positive or negative) for the elements of $\theta$
gives rise to physically feasible values for $V$, $k_{e}$ and
$k_{a}$ and generates a \textit{distinct} concentration-time
profile.




\section{System set-up}


Before doing anything, you will need to install some software.

Please read the document ``Use of BlackBox with OpenBUGS'', and
follow the instructions.






\section{New module -- ``UDevScalarTemplate''}
Now start your copy of BlackBox and open the new module:
\begin{eqnarray}
\tt{OpenBUGS/UDev/Mod/ScalarTemplate.odc}
\nonumber
\end{eqnarray}
assuming that OpenBUGS has been installed to the folder \texttt{OpenBUGS}.

Note that to reduce the risk of errors creeping into the system we
recommend that all other new components are also stored in the
\texttt{UDev/Mod} directory. As the name suggests, the
\texttt{ScalarTemplate} module can be used as a template for such
new components, so long as they represent scalar-valued functions
(see later for details regarding vector-valued functions). Please
note that only those parts of the code that are currently marked
in blue should be modified. The following notes pertain to areas
of code labelled with the corresponding numbers within comment
markers~\texttt{(*} and \texttt{*)}:
\begin{verbatim}
    (* this is a comment in Component Pascal *)
\end{verbatim}
\begin{enumerate}
\item[\texttt{(*1*)}] The first line of a Component Pascal module
should always read \texttt{MODULE}, followed by the module's name,
in this case \texttt{UDevScalarTemplate}, followed by a
semi-colon. The last line of the module should read \texttt{END},
followed by the module's name, followed by a \emph{full stop}
(period). All new module names for new components of this type
should begin with \texttt{UDev}; the corresponding \emph{file}
names should be identical but with the \texttt{UDev} prefix
removed (they must also begin with at least one capital letter);
all new files of this type should be saved in the
\texttt{UDev/Mod} directory.

\item[\texttt{(*2*)}] Various other
modules can be `imported' into each new module, which means that
procedures and/or data structures defined in those modules can be
used/exploited from within the new one. The \texttt{Math}
module is an integral part of the BlackBox software that
defines many fundamental mathematical functions, which are called
from within other modules via the syntax \texttt{Math.}~followed
by the relevant procedure name, e.g.~\texttt{Math.Ln(.)}~for
natural logarithms, \texttt{Math.Exp(.)}~for exponentials. see
the \texttt{Evaluate} procedure of this module for examples
of their use.

Documentation regarding the \texttt{Math} module can be accessed
by highlighting the word \texttt{Math} in BlackBox and selecting
\texttt{Documentation} from the \emph{second} \texttt{Info} menu
(the first \texttt{Info} menu belongs to OpenBUGS).



\item[\texttt{(*3*)}] The \texttt{Signature(.)}
procedure is used to declare the types of
arguments required to define the function of interest. In the case
of our one compartment pharmacokinetic model defined in
Eq.~\ref{PKFO1Equation} above, the required arguments are: the
parameter vector $\theta$, the dose $D$, and the time $t$. Thus we
have a vector followed by two scalars and so we set the
\texttt{signature} variable equal to \texttt{"vss"} (\texttt{v} for
vector; \texttt{s} for scalar).



\item[\texttt{(*4*)}] The \texttt{Evaluate}
procedure is used to define a variable called \texttt{value},
which stores the function's current value (given the current
values of its arguments). Throughout the procedure we make use of
a variable called \texttt{func}, which represents the function
itself. In particular, we refer several times to one of its
`internal' fields, \texttt{arguments}. This is an `irregular'
matrix where each row corresponds to one of the arguments declared
in \texttt{Signature(.)}~above (in the same order). If a
particular argument is a vector (\texttt{v}) then the length of
the corresponding row of \texttt{func.arguments} is equal to the
length of that vector, whereas if an argument is a scalar
(\texttt{s}) then the length of the corresponding row is 1. The
procedure call \texttt{func.arguments[i][j].Value()} returns the
value of the \texttt{j}th element of the \texttt{i}th argument.
Thus the value of $\log k_e$, for example, can be
obtained via the call \texttt{func.arguments[0][1].Value()} --
because $\log k_e$ is the second element (index = 1) of the
$\theta$ vector, which is the function's first argument (index =
0).

{\bf Note:} Array indices start at 0 in Component Pascal rather
than 1. Thus if the array has $N$ elements, the array index will
have values $i = 0, ..., N-1$.

\item[\texttt{(*5*)}] Here we define three
constants that allow us to index the various rows of
\texttt{func.arguments} via meaningful names rather than directly
by the relevant numbers themselves, i.e.~we can use the names
\texttt{parameters}, \texttt{dose} and \texttt{time} in place of
\texttt{0}, \texttt{1} and \texttt{2} to access the function's
first, second and third arguments, respectively. This is by no
means essential but \emph{is} considered to be `good practice' as
it reduces the likelihood of coding errors arising.


\item[\texttt{(*6*)}] Note that any number of
`local' variables can be declared and used to aid in specifying
the new function, so long as their names do not clash with other
variable/procedure names -- the compiler (\texttt{Ctrl+K}) will
normally inform the programmer of any errors.


\item[\texttt{(*7*)}] This is a standard
``IF/THEN/ELSE'' statement in Component Pascal; note that the
``ELSE'' branch can be omitted where appropriate -- see below.


\item[\texttt{(*8*)}] This is a standard ``IF''
statement in Component Pascal -- here we simply set equal to zero
any negligibly small values that have been calculated as negative
due to machine precision errors.
\end{enumerate}


\textbf{Please note that (almost) every Component Pascal statement
ends with a semi-colon.} Hopefully this brief example demonstrates
sufficient use of the Component Pascal syntax that the reader is
able to begin writing their own modules from this template.
Detailed documentation on both BlackBox and the Component Pascal
language can be accessed via the \texttt{Help} menu.

Further insight may also be gained by examining our second
template, which shows how to implement new components to represent
vector-valued functions -- see later. Please read the instructions
below before attempting to write your own modules. In addition, 
there are several further examples outlined towards the end of this 
document. 











\section{Using ``UDevScalarTemplate'' as a template}

The following instructions should
be followed closely when defining a new BUGS function via the
\texttt{UDevScalarTemplate} template:
\begin{enumerate}
\item[1.] Choose a name for the new component,
\texttt{NewFunction}, say (the new name \emph{must} begin with a
capital letter). Start your copy of BlackBox and open the
\texttt{UDev/Mod/ScalarTemplate.odc} template from within it;
then save the template under the new name,
for example
\begin{verbatim}
  UDev/Mod/NewFunction.odc
\end{verbatim}
{\bf Do not
overwrite an existing module!} Now modify the module name both
at the top and at the bottom of the new file -- change these from
\texttt{UDevScalarTemplate} to \texttt{UDev} followed by the new
component's name, e.g.~\texttt{UDevNewFunction}. Save and compile
the new component by pressing \texttt{Ctrl+S} (save) followed by
\texttt{Ctrl+K} (compile) -- there should be no compilation errors
at this stage since only the module name has been changed.


\item[2.] Now modify the code in the new module according to the
desired function:
\begin{enumerate}
\item declare the types of arguments required in the
\texttt{Signature()} method at the line labelled \texttt{(*3*)}, and
\item redefine the
\texttt{Evaluate(.)}~procedure starting at line \texttt{(*4*)}.
\end{enumerate}
You can save the new module at any
time by pressing \texttt{Ctrl+S} (or by selecting \texttt{Save}
from the \texttt{File} menu). You can also attempt to compile the
code at any time by pressing \texttt{Ctrl+K} (or by selecting
\texttt{Compile} from the \texttt{Dev} menu). If there are any
compilation errors when you attempt to compile your code, each one
will be marked in the code by a grey box with a white cross
running through it. An error message pertaining to the first error
will be displayed on the status bar (which lies across the bottom
of the BlackBox `program window') and the cursor should
automatically position itself next to the corresponding grey box.
We advise that you deal with any compilation errors in order, but
if for some reason this makes things awkward (or is not possible)
then error messages for specific compilation errors can be
obtained by clicking on the appropriate grey boxes -- a single
click shows the error message on the status bar whereas
double-clicking reveals it within the code, in place of the grey
box (double-click again to revert back to the grey box).



\item[3.]
Once the new module has been successfully compiled (and saved)
then it can be `linked' into the OpenBUGS software by modifying the
file \texttt{UDev/Mod/External.odc}. This file contains \texttt{Register}
commands for every new function to be added to the BUGS language.

For the \texttt{ScalarTemplate} example, the command is:

\texttt{Register("udev.scalartemplate", "UDevScalarTemplate.Install");}

where the first string is the name to be used in the BUGS language,
and the second is the procedure in the module which BUGS uses to
access the function.

In the case of the new function in the module \texttt{UDevNewFunction}
the command would be:

\texttt{Register("my.new.function", "UDevNewFunction.Install");}

In order to use the new function, the file \texttt{External.odc} must
be compiled (Control-K), and you must exit from BlackBox. The function
will be available when BlackBox is started again.

%The notation is described as follows: `\texttt{s}' means that the
%component represents a scalar as opposed to a vector (\texttt{v}),
%whereas `\texttt{<-}' indicates that the component represents a
%function as opposed to a distribution ($\tt{\sim}$);
%`\texttt{one.comp.pk.model}' is the name chosen for specifying the
%new function via the BUGS language; `\texttt{(v, s, s)}' tells the
%system that the function requires one vector and two scalar
%arguments (in that order) as declared in the
%\texttt{DeclareArgTypes(.)}~procedure; and
%`\texttt{UDevScalarTemplate.Install}' is the `installation'
%procedure for the component -- this is defined towards the bottom
%of the template module. Make a copy of this first line immediately
%beneath it, and replace \texttt{one.comp.pk.model} on the
%\emph{new} line with the desired BUGS-language name for your new
%function, e.g.~\texttt{new.function} (this is the name with which
%you wish to refer to the new function during future OpenBUGS
%sessions). Now declare the new function's arguments and specify
%the name of the module where its `installation' procedure can be
%found -- replace \texttt{UDevScalarTemplate} with your new
%module's name, e.g.~\texttt{UDevNewFunction}. You should end up
%with something like

%\texttt{s <- "one.comp.pk.model"(v, s,
%s)~~~~~~~~~~~~~~~"UDevScalarTemplate.Install" \\
%s <- "new.function"(s,
%v)~~~~~~~~~~~~~~~~~~~~~~~"UDevNewFunction.Install"}

%at the top of the \texttt{UDev/Rsrc/Functions.odc} file. Now save
%the new \texttt{UDev/Rsrc/Functions.odc} file -- the new
%component will be available from the next time that BlackBox is
%started, so don't forget to shut down the software before trying
%to use your new function.

In this example the new function could be accessed in a model
via BUGS language code of the following form:
\begin{verbatim}
  model {
    #  ...
    value <- my.new.function(x, par[1:p])
    #  ...
  }
\end{verbatim}


\end{enumerate}
















\section{Vector-valued functions}

Vector-valued functions can be `hard-wired' into the system by
making use of a different template, which can be found in
\texttt{UDev/Mod/VectorTemplate.odc}. Here we define a version of
our one compartment pharmacokinetic model that is now
vector-valued by virtue of the fact that we now wish to evaluate
it at a vector of times rather than a single time. Except for a
few minor points discussed below, the details of implementation
are exactly analogous to those for scalar-valued functions.
\begin{enumerate}
\item[\texttt{(*1*)}] Module name.

\item[\texttt{(*2*)}] The \texttt{args} variable is now set equal
to \texttt{"vsv"} rather than \texttt{"vss"} since the function's
third argument has changed from a single time value to a vector of
time values.

\item[\texttt{(*3*)}] The
\texttt{Evaluate(.)}~procedure must now return \emph{an array} of
values, via the `\texttt{values}' variable, rather than a scalar
(previously via `\texttt{value}').

\item[\texttt{(*4*)}]
Component Pascal's \texttt{LEN(.)}~function returns the length of
the specified argument, so long as that argument is a
one-dimensional array: thus \texttt{LEN(func.arguments[times])} is
the number of times at which the function is to be evaluated. If
the array has more than one dimension then \texttt{LEN(.)}~returns
the length of the \emph{first} dimension -- see the BlackBox
documentation for more details.


\item[\texttt{(*5*)}--\texttt{(*7*)}] This section forms the basic
structure of a Component Pascal
\texttt{WHILE}-loop. At line \texttt{(*6*)} the command  \texttt{INC(i)}
increments the value of \texttt{i} by one.
During each `pass' through the loop,
the \texttt{t} variable is set equal to one of the times at which
the function is to be evaluated and the corresponding evaluation
is performed by making use of the `temporary' variable
\texttt{val}. At the end of each pass, \texttt{values[i]} ($\tt{i
= 0,...,numTimes-1}$) is set equal to \texttt{val}.
\end{enumerate}
Finally, the function must be associated with a string to enable it to be
called in the BUGS language, to be put into the file \texttt{External.odc}
using the statement:
\begin{verbatim}
Register("udev.vectortemplate", "UDevVectorTemplate.Install");
\end{verbatim}











\appendixpage
These appendices contain brief descriptions of some functions created using the
templates described above, and are included in the OpenBUGS distribution. Modules are
included for all of them and so may be used for reference.

\appendix




\section{DJLPKIVbol2, DJL2CompDisp}
\begin{figure}[h]
\centering
\LargePicture\VCDraw{
%\ShowFrame\ShowGrid % comment these out to get rid of the grid and bounding box of the figure
\begin{VCPicture}{(0,0)(10,5)}

% states
\HideState \State{(2.0, 5.5)}{c} \ShowState
\LargeState\StateVar[A_1 = C_1 V_1]{(2, 2.5)}{A}
\StateVar[A_2 = C_2V_2]{(8, 2.5)}{B}
\HideState \State{(2.0, -0.5)}{d}\ShowState

% edges
\ArcL{A}{B}{k_{12}}
\ArcL{B}{A}{k_{21}}
\EdgeR{c}{A}{RI}
\EdgeR{A}{d}{k_{10}}

\end{VCPicture}
}
\caption{Two compartment pharmacokinetic model with input into
($RI$) and elimination from ($k_{10}$) the central compartment
only. Compartmental volumes, concentrations, and amounts of drug
are denoted by $V_i$, $C_i$ and $A_i$, respectively.}
\label{2CompartmentModel}
\end{figure}
Source code in the files:
\begin{verbatim}
  UDev/Mod/DJLPKIVbol2.odc
  UDev/Mod/DJL2CompDisp.odc
\end{verbatim}
Consider the two compartment pharmacokinetic model shown in
Fig.~\ref{2CompartmentModel}. This can be expressed
mathematically as
\begin{eqnarray}
\frac{\mbox{d}A_1}{\mbox{d}t} &=& RI + k_{21}A_{2} - k_{12}A_{1} - k_{10}A_{1} \label{EqComp1} \\
\frac{\mbox{d}A_2}{\mbox{d}t} &=& k_{12}A_{1} - k_{21}A_{2}
\label{EqComp2}
\end{eqnarray}
For the case in which the input function ($RI$) represents an
intravenous bolus, i.e.~where all of the dose is injected into
venous blood as quickly as possible: $RI = 0, A_{1}(0_{+})=D$
(where $D$ denotes the administered dose), the concentration of
drug in the central compartment at time $t$ is given by
\begin{equation}
C_{1}(t)=\frac{D}{V_{1}} \left\{ A \mbox{e}^{-\lambda_{1}t} + (1 -
A) \mbox{e}^{-\lambda_{2}t} \right\}
\end{equation}
where (after some algebra):
\begin{eqnarray}
\lambda_1 = \frac{1}{2} \left \{ k_{12} + k_{21} + k_{10} + \sqrt{(k_{12} + k_{21} + k_{10})^2 - 4k_{10}k_{21}} \right \}; \nonumber \\
\lambda_2 = k_{12} + k_{21} + k_{10} - \lambda_1; \nonumber \\
\mbox{and } A = \frac{\lambda_1 - k_{21}}{\lambda_1 - \lambda_2}.
\nonumber
\end{eqnarray}
For the purposes of Bayesian inference, arguably the most
appropriate parameterisation for this model is in terms of the
following `disposition' parameters:
\begin{eqnarray}
\theta = \log (CL, Q, V_{1}, V_{2}) \nonumber
\end{eqnarray}
where $CL = k_{10}V_{1}$ and $Q=k_{12}V_{1}=k_{21}V_{2}$. This is
because each possible combination of real values (positive or
negative) for the elements of $\theta$ is then not only physically
feasible but also gives rise to a \emph{distinct}
concentration-time profile. Thus an assumption of multivariate
normality for $\theta$ is appropriate.

The \texttt{UDevDJLPKIVbol2} component takes values of $t$, $D$
and $\psi = (\lambda_1 , \lambda_2 , A , V_{1})$ as arguments (in
the order $\psi$, $t$, $D$) and returns the corresponding value of
$C_1$. An example of its usage is as follows:
\begin{eqnarray}
\mbox{\texttt{conc <- udev.djlivbol2(psi[1:4], time, dose)}}
\nonumber
\end{eqnarray}
The component is designed to be used in
conjunction with the \texttt{DJL2CompDisp} component,
although this is not essential.

The \texttt{UDevDJL2CompDisp} component maps the current value of
$\theta$ to
\begin{eqnarray}
\psi = (\lambda_1 , \lambda_2 , A , V_{1}), \nonumber
\end{eqnarray}
which is useful in the evaluation of \emph{all} standard two
compartment models. This component is then used in OpenBUGS as:
\begin{eqnarray}
\mbox{\texttt{psi[1:4] <- udev.djl2compdisp(theta[1:4])}} \nonumber
\end{eqnarray}










\section{DJLPKIVbol3, DJL3CompDisp}
\begin{figure}[h]
\centering
\LargePicture\VCDraw{
%\ShowFrame\ShowGrid % comment these out to get rid of the grid and bounding box of the figure
\begin{VCPicture}{(0,0)(12,5)}

% states
\HideState \State{(1.0, 4)}{c} \ShowState
\LargeState\StateVar[A_1 = C_1 V_1]{(6, 4)}{A}
\StateVar[A_2 = C_2V_2]{(2.5, 0.5)}{B}
\StateVar[A_3 = C_3V_3]{(9.5, 0.5)}{C}
\HideState \State{(11, 4.0)}{d}\ShowState

% edges
\ArcL{A}{B}{k_{12}}
\ArcL{B}{A}{k_{21}}
\ArcL{C}{A}{k_{31}}
\ArcL{A}{C}{k_{13}}
\EdgeL{c}{A}{RI}
\EdgeL{A}{d}{k_{10}}

\end{VCPicture}
}
\caption{Three compartment pharmacokinetic model with input into
($RI$) and elimination from ($k_{10}$) the central compartment
only. Compartmental volumes, concentrations, and amounts of drug
are denoted by $V_i$, $C_i$ and $A_i$, respectively.}
\label{3CompartmentModel}
\end{figure}
Source code in the files:
\begin{verbatim}
  UDev/Mod/DJLPKIVbol3.odc
  UDev/Mod/DJL3CompDisp.odc
\end{verbatim}
Consider the three compartment pharmacokinetic model shown in
Fig.~\ref{3CompartmentModel} above. This can be expressed
mathematically via:
\begin{eqnarray}
\frac{\mbox{d}A_1}{\mbox{d}t} &=& RI + k_{21}A_{2} + k_{31}A_{3} -
k_{12}A_{1} - k_{13}A_{1} - k_{10}A_{1} \label{EqComp1} \\
\frac{\mbox{d}A_2}{\mbox{d}t} &=& k_{12}A_{1} - k_{21}A_{2}
\label{EqComp2} \\
\frac{\mbox{d}A_3}{\mbox{d}t} &=& k_{13}A_{1} -
k_{31}A_{3} \label{EqComp3}
\end{eqnarray}
For the case in which the input function ($RI$) represents an
intravenous bolus, i.e.~where all of the dose is injected into
venous blood as quickly as possible: $RI = 0, A_{1}(0_{+})=D$
(where $D$ denotes the administered dose), the concentration of
drug in compartment $1$ at time $t$ is given by
\begin{equation}
C_{1}(t)={\frac{D}{V_{1}}} \left\{ A \mbox{e}^{-\lambda_{1}t} + B
\mbox{e}^{-\lambda_{2}t} + (1 - A - B) \mbox{e}^{-\lambda_{3}t}
\right\}
\end{equation}
where (after considerable algebra):
\begin{eqnarray}
A & =& (k_{31}-\lambda_1)(k_{21}-\lambda_1)/(\lambda_2 - \lambda_1)(\lambda_3 - \lambda_1), \\
B & =&(k_{31} - \lambda_2)(k_{21} - \lambda_2)/(\lambda_1 - \lambda_2)(\lambda_3 - \lambda_2)
\end{eqnarray}
\begin{equation}
\lambda_1 = \max (\lambda_{1}' , \lambda_{2}' , \lambda_{3}'), \;\;\;\;
\lambda_2 = \mbox{median} (\lambda_{1}' , \lambda_{2}' , \lambda_{3}')
\;\;\;\; \lambda_3 = \min (\lambda_{1}' , \lambda_{2}'
, \lambda_{3}')
\end{equation}
\begin{equation}
\lambda_i' = {\scriptsize\frac{\alpha}{3}} - 2 \sqrt{-{\scriptsize\frac{a}{3}}} \times
\cos\left\{(\phi + 2 (i - 1) \pi)/3 \right\}, \quad i = 1,2,3
\end{equation}
\begin{equation}
\phi = \cos^{-1} \left( -b /2 \sqrt{-a^3/27}\right), \quad
a = \beta - \frac{1}{3}\alpha^2, \quad b = \frac{2}{27} \alpha^3
- \frac{1}{3} \alpha \beta + \gamma
\end{equation}
\begin{equation}
\alpha = k_{13} + k_{12} + k_{10} + k_{31} + k_{21}, \quad \beta =
k_{31} (k_{12} + k_{10} + k_{21}) + k_{21} (k_{13} + k_{10}),
\end{equation}
and $\gamma = k_{21}k_{31}k_{10}$.
The \texttt{DevDJLPKIVbol3} component takes as arguments
values of
and $\psi = (\lambda_1 , \lambda_2 , \lambda_3 , A , B , V_{1}),$
$t$ and $D$ (in that order) and returns the
corresponding value of $C_1(t)$. An example of its usage in BUGS
language code would be
\begin{verbatim}
  conc <- udev.djlivbol3(psi[1:6], time, dose)
\end{verbatim}

For the purposes of Bayesian inference, arguably the most
appropriate parameterisation for this model is in terms of the
following `disposition' parameters:
\begin{eqnarray}
\theta = \log (CL, Q_{12}, Q_{13}, V_{1}, V_{2}, V_{3} - V_{2})
\nonumber
\end{eqnarray}
where $CL = k_{10}V_{1}$, $Q_{12}=k_{12}V_{1}=k_{21}V_{2}$, and
$Q_{13}=k_{13}V_{1}=k_{31}V_{3}$. This is because each possible
combination of real values (positive or negative) for the elements
of $\theta$ is then not only physically feasible but also gives
rise to a \emph{distinct} concentration-time profile. Thus an
assumption of multivariate normality for $\theta$ is appropriate.

The \texttt{UDevDJL3CompDisp} component maps the current value of
$\theta$ to
\begin{eqnarray}
\psi = (\lambda_1 , \lambda_2 , \lambda_3 , A , B , V_{1}),
\nonumber
\end{eqnarray}
which is useful in the evaluation of \emph{all} standard three
compartment models. An example of the new component's usage (from
within OpenBUGS) is as follows:
\begin{verbatim}
  psi[1:6] <- udev.djl3compdisp(theta[1:6])
\end{verbatim}


















\section{Elicitor Piecewise Function }
Source code in the file:
\begin{verbatim}
  UDev/Mod/ElicitorPiecewise.odc
\end{verbatim}
\begin{figure}
\centering
\includegraphics[scale= 1.0, width=10cm, bb = 0 0 600 331 ]{FigureELP.bmp}
\caption{Figure for ELP}
\label{FigureForELP}
\end{figure}

{\scriptsize Mary Kynn, Department of Mathematics and Statistics, Fylde College,
Lancaster University, \\ (\texttt{m.kynn@lancaster.ac.uk}), February 13, 2004}

Consider the piecewise continuous linear function shown in figure Figure~\ref{FigureForELP}).
It is anchored at some maximum value and then constructed outwards from the maximum, where each
section is given a gradient and begin/end points (knots). For example
\begin{equation}
f(x) = \begin{cases}
s_0(x - 40) + s_1(40 - 50) + s_2(50 - 60) 	,\hfill 0 < x \leq 40.0 \\
s_1(x - 50) + s_2(50 - 60) ,\hfill 40 < x \leq 50 \\
s_2(x - 60) 	, \hfill 50 < x \leq 60\\
s_3(x - 60) 	, \hfill 60 < x \leq 75 \\
s_4(x - 75) + s_3 (75 - 60) 	, \hfill 75 < x \leq 81 \\
s_5 (x - 81) + s_4 (81 - 75) + s_3 (75 - 60) 	, \hfill 81< x \leq 100
\end{cases}
\end{equation}
The piecewise function can be called from OpenBUGS by the command
\begin{verbatim}
  variable <- udev.elicitorpiecewise(x, numberKnots,
                                     xmaxKnot, slopes[], knots[])
\end{verbatim}
So for the example described above
\begin{verbatim}
  numberKnots = 7
  xmaxKnot = 3
  slopes = [s0, s1, s2, s3, s4, s5]
  knots = [0, 40, 50, 60, 75, 81, 100]
\end{verbatim}
The function tests to see in which piece the argument \texttt{x} lies
and returns the corresponding value.





\section{SparseMatrixVectorProduct}

{\scriptsize Juan J. Abell\'{a}n,
Department of Epidemiology and Public Health, Imperial College London, \\
(\texttt{j.abellan@imperial.ac.uk}), 5 October 2004.}

Source code in the file:
\begin{verbatim}
  UDev/Mod/SparseMatrixVectorProduct.odc
\end{verbatim}


The module \texttt{UDevSparseMatrixVectorProduct} performs the
multiplication $Av$ of a $n\times m$ sparse matrix $A$ and
a vector $v \in \mathbb{R}^m$.
One of the many possible representations
is the compressed sparse row format. Using this method,
the matrix $A$ is stored in the following way:
\begin{enumerate}
\item \texttt{ra}: a real array of $s$ elements containing the non-zero
elements of $A$, stored in row order. Thus, if $i < j$, all elements of row $i$
 precede elements from row $j$. The order of elements within
the rows is immaterial.

\item \texttt{ja}: an integer array of $s$ elements containing the column
    indices of the elements stored in \texttt{ra}.

\item \texttt{ia}: an integer array of $n + 1$ elements containing pointers
to the beginning of each row in the arrays \texttt{ra} and \texttt{ja}.
Thus \texttt{ia[i]} indicates the position in the arrays \texttt{ra} and
\texttt{ja} where the ith row begins. The last $(n+1)$-th element of
\texttt{ia} indicates where the $n+1$ row would start, if it existed.
\end{enumerate}
This module is associated with the function name
\texttt{udev.sparsemat}, and its arguments are the
three vectors \texttt{ra}, \texttt{ja}, and \texttt{ia} characterizing
$A$, and the vector $v$. It should be called in the following way:
\begin{verbatim}
  u[1:m] <- udev.sparsemat(ra[], ja[], ia[], v[])
\end{verbatim}
Note the dimension \texttt{1:m} within the square brackets in the
left-hand side of the assignment.































\end{document}
