### BUGSModelLibrary is a prototype PKPD model library for use with WinBUGS 1.4.3 ###

The current version includes:

* Speciﬁc linear compartmental models:
      * One compartment model with ﬁrst order absorption
      * Two compartment model with elimination from and ﬁrst order absorption into central compartment
* General linear compartmental model described by a matrix exponential
* General compartmental model described by a system of ﬁrst order ODEs

The models and data format are based on NONMEM/NMTRAN/PREDPP conventions including:

* Recursive calculation of model predictions
      * This permits piecewise constant covariate values
* Bolus or constant rate inputs into any compartment 
* Handles single dose, multiple dose and steady-state dosing histories
* Implemented NMTRAN data items include:
      * TIME, EVID, CMT, AMT, RATE, ADDL, II, SS

The library provides BUGS language functions that calculate amounts in each compartment. BUGSModelLibrary is an active, open-source project of Metrum Research Group (http://www.metrumrg.com).